package com.khongdet.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRootSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldCreateRootSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRootUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }


    @Test
    public void shouldRootUpFailAtMin() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MIN);
        boolean result = robot.up();
        assertEquals(false, result);
        assertEquals(Robot.Y_MIN, robot.getY());
    }
    

    @Test
    public void shouldRootUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }

    @Test
    public void shouldRootUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }


    @Test
    public void shouldRootUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRootDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldRootDownBeforeMAXSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX-1);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }


    @Test
    public void shouldRootDownAtMAX() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX);
        boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }

    @Test
    public void shouldRootDownNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(16, robot.getY());
    }

    @Test
    public void shouldRootDownNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(8);
        assertEquals(true, result);
        assertEquals(19, robot.getY());
    }


    @Test
    public void shouldRootDownNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(12);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldRobotLeftSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(9, robot.getX());
    }


    @Test
    public void shouldRobotLeftBeforeMINSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN + 1, 10);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(Robot.X_MIN, robot.getX());
    }

    @Test
    public void shouldRobotLeftFailAtMIN() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN, 10);
        boolean result = robot.left();
        assertEquals(false, result);
        assertEquals(Robot.X_MIN, robot.getX());
    }

    @Test
    public void shouldRootLeftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left(5);
        assertEquals(true, result);
        assertEquals(5, robot.getX());
    }

    @Test
    public void shouldRootLeftNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left(10);
        assertEquals(true, result);
        assertEquals(0, robot.getX());
    }


    @Test
    public void shouldRootLeftNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left(12);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }


    @Test
    public void shouldRobotRight() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(11, robot.getX());
    }

    @Test
    public void shouldRobotRightBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX - 1, 10);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }

    @Test
    public void shouldRobotRightFailAtMAX() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX, 10);
        boolean result = robot.right();
        assertEquals(false, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }

    @Test
    public void shouldRootRightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right(5);
        assertEquals(true, result);
        assertEquals(15, robot.getX());
    }

    @Test
    public void shouldRootRightNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right(9);
        assertEquals(true, result);
        assertEquals(19, robot.getX());
    }


    @Test
    public void shouldRootRightNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right(12);
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }
}
