package com.khongdet.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank khongdet = new BookBank("Khongdet", 100.0);
        khongdet.print();
        khongdet.deposit(50);
        khongdet.print();
        khongdet.withdraw(50);
        khongdet.print();

        BookBank shiba = new BookBank("Shiba", 1);
        shiba.deposit(1000000);
        shiba.withdraw(10000000);
        shiba.print();

        BookBank pig = new BookBank("Pig", 10);
        pig.deposit(10000000);
        pig.withdraw(1000000);
        pig.print();
    }
}
